package sa.ntis.ameen.sahalat;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView paymentsRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA}, 1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        final Intent i = new Intent(this, QRReader.class);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                startActivityForResult(i,0);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        paymentsRecyclerView = (RecyclerView) findViewById(R.id.payments_RecycleView);
        List<Payment> arrayOfFiles = new ArrayList<Payment>();

        Payment amFile = new Payment("Albaik Resturant - 744","0","16.75 SAR");
        amFile.setId( 0);
        arrayOfFiles.add(amFile);
        amFile = new Payment("Albaik Resturant - Mina","0","48.25 SAR");
        amFile.setId( 1 );
        arrayOfFiles.add(amFile);
        amFile = new Payment("Al Mashaar Train","1","44 SAR");
        amFile.setId( 2 );
        arrayOfFiles.add(amFile);
        amFile = new Payment("Ajmal perfumes","3","112.31 SAR");
        amFile.setId( 3 );
        arrayOfFiles.add(amFile);
        amFile = new Payment("Makkah Taxi Company","2","33.99 SAR");
        amFile.setId( 4);
        arrayOfFiles.add(amFile);
        amFile = new Payment("Subway food","0","69.75  SAR");
        amFile.setId( 5);
        arrayOfFiles.add(amFile);
        amFile = new Payment("Panda store - 16 LR","3","365 SAR");
        amFile.setId( 6);
        arrayOfFiles.add(amFile);
        amFile = new Payment("Al tazij resturant makkah","1","415 SAR");
        amFile.setId( 7);
        arrayOfFiles.add(amFile);

        ((RecyclerView)paymentsRecyclerView).setAdapter(new PaymentsAdapter(this , arrayOfFiles));
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(paymentsRecyclerView.getContext(),0);
        paymentsRecyclerView.addItemDecoration(dividerItemDecoration);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        //super.onActivityResult( requestCode, resultCode, data );
        Log.w( "ntis", "# " + String.valueOf( resultCode ) );
        ((TextView)findViewById( R.id.walletBalance )).setText(
                String.valueOf(Integer.parseInt( ((TextView)findViewById( R.id.walletBalance )).getText().toString() )-
                 resultCode ));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            final Intent i2 = new Intent(this, QRReader.class);
            startActivity(i2);
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class Payment {
        private String name;
        private String type;
        private String amount;

        public void setId(int id) {
            this.id = id;
        }

        private int id;


        public Payment(String name, String type, String amount) {
            this.name = name;
            this.type = type;
            this.amount = amount;

        }
        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public String getAmount() {
            return amount;
        }
    }
    public static class PaymentsAdapter
            extends RecyclerView.Adapter<PaymentsAdapter.ViewHolder> {

        private final MainActivity mParentActivity;
        private final List<Payment> mValues;
        int selectedPosition = -1;
        private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.w("ntis", "item");
                Payment item = (Payment) view.getTag();
                selectedPosition = Integer.valueOf(item.getId());
                Log.w( "ntis", "--" + String.valueOf( selectedPosition ) );
                notifyDataSetChanged();
            }
        };

        PaymentsAdapter(MainActivity parent, List<Payment> items) {
            Log.w("aaaa", String.valueOf(items.size()));
            mValues = items;
            mParentActivity = parent;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            if (selectedPosition == position)
                holder.itemView.setBackgroundColor(Color.parseColor("#dbdbdb"));
            else
                holder.itemView.setBackgroundColor(Color.TRANSPARENT);

            Payment file = mValues.get(position);
            Log.w("ntis", file.getName());
            holder.itemView.setTag(mValues.get(position));
            holder.titleView.setText(file.getName());
            holder.sizeView.setText(file.getAmount());
            if(file.getType()=="0") {
                holder.icon.setBackgroundResource(R.drawable.ic_restaurant_menu);
            } else  if(file.getType()=="1") {
                holder.icon.setBackgroundResource(R.drawable.ic_subway);

            } else  if(file.getType()=="2") {
                holder.icon.setBackgroundResource(R.drawable.ic_local_taxi);

            } else  if(file.getType()=="3") {
                holder.icon.setBackgroundResource(R.drawable.ic_store_mall_directory);

            }
            holder.itemView.setOnClickListener(mOnClickListener);
        }


        @Override
        public int getItemCount() {
            return mValues.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            final TextView titleView;
            final TextView sizeView;
            final ImageView icon;

            ViewHolder(View view) {
                super(view);
                titleView = view.findViewById(R.id.selectedRowFileTitle);
                sizeView = view.findViewById(R.id.selectedRowFileSize);
                icon = view.findViewById(R.id.selectedRowIcon);

            }
        }
    }

}
