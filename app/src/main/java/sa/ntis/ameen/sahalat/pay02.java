package sa.ntis.ameen.sahalat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class pay02 extends AppCompatActivity {
    private Button pay = null;
    private Button close = null;

    LinearLayout doneLinearLayout;
    LinearLayout keypad;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay02);
        setTitle("Pay");

        pay = findViewById(R.id.pay2);
        close = findViewById(R.id.closeActivity2);
        keypad = findViewById(R.id.keypad2);

        doneLinearLayout = findViewById(R.id.doneLinearLayout2);
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doneLinearLayout.setVisibility(View.VISIBLE);
                doneLinearLayout.setAlpha(0.0f);
                doneLinearLayout.animate()
                        .alpha(1.0f)
                        .setListener(null);

                keypad.setVisibility(View.GONE);
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Integer.valueOf( 366 ));
                finish();
            }
        });


    }
}
