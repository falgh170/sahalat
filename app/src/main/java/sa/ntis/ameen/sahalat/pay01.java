package sa.ntis.ameen.sahalat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

public class pay01 extends AppCompatActivity {
    private EditText f_field = null;
    private TextView t9_key_0 = null;
    private TextView t9_key_1 = null;
    private TextView t9_key_2 = null;
    private TextView t9_key_3 = null;
    private TextView t9_key_4 = null;
    private TextView t9_key_5 = null;
    private TextView t9_key_6 = null;
    private TextView t9_key_7 = null;
    private TextView t9_key_8 = null;
    private TextView t9_key_9 = null;
    private TextView clear = null;
    private Button pay = null;
    private Button close = null;

    LinearLayout doneLinearLayout;
    LinearLayout keypad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_pay01 );
        setTitle( "Pay" );
        f_field = findViewById( R.id.f_field );
        t9_key_0 = findViewById( R.id.t9_key_0 );
        t9_key_1 = findViewById( R.id.t9_key_1 );
        t9_key_2 = findViewById( R.id.t9_key_2 );
        t9_key_3 = findViewById( R.id.t9_key_3 );
        t9_key_4 = findViewById( R.id.t9_key_4 );
        t9_key_5 = findViewById( R.id.t9_key_5 );
        t9_key_6 = findViewById( R.id.t9_key_6 );
        t9_key_7 = findViewById( R.id.t9_key_7 );
        t9_key_8 = findViewById( R.id.t9_key_8 );
        t9_key_9 = findViewById( R.id.t9_key_9 );
        clear = findViewById( R.id.t9_key_clear );
        pay = findViewById( R.id.pay );
        close = findViewById( R.id.closeActivity );
        keypad = findViewById( R.id.keypad );

        t9_key_0.setOnClickListener( mOnClickListener );
        t9_key_1.setOnClickListener( mOnClickListener );
        t9_key_2.setOnClickListener( mOnClickListener );
        t9_key_3.setOnClickListener( mOnClickListener );
        t9_key_4.setOnClickListener( mOnClickListener );
        t9_key_5.setOnClickListener( mOnClickListener );
        t9_key_6.setOnClickListener( mOnClickListener );
        t9_key_7.setOnClickListener( mOnClickListener );
        t9_key_8.setOnClickListener( mOnClickListener );
        t9_key_9.setOnClickListener( mOnClickListener );

        doneLinearLayout = findViewById( R.id.doneLinearLayout );
        pay.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doneLinearLayout.setVisibility( View.VISIBLE );
                doneLinearLayout.setAlpha( 0.0f );
                doneLinearLayout.animate()
                        .alpha( 1.0f )
                        .setListener( null );

                keypad.setVisibility( View.GONE );
            }
        } );
        close.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Log.w( "ntis", "pay011 " + String.valueOf( f_field.getText() ) );
                    setResult( Integer.valueOf( String.valueOf( f_field.getText() ) ) );
                } catch (Exception ex) {
                    setResult( 0 );
                }
                finish();
            }
        } );
        clear.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                f_field.setText( "" );
            }
        } );
    }

    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TextView tv = (TextView) view;
            f_field.setText( (f_field.getText() + String.valueOf( tv.getText() )) );
            Log.w( "ntis", "pay01 " + String.valueOf( tv.getText() ) );
        }
    };
}
